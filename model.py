import pandas as pd
import numpy as np
import argparse


def area_triangle(x1, y1, x2, y2, x3, y3):
    return abs(0.5 * (x1 * (y2 - y3) + x2 * (y3 - y1) + x3 * (y1 - y2)))


def perimeter_triangle(x1, y1, x2, y2, x3, y3):
    return (np.sqrt(np.square(x1 - x2) + np.square(y1 - y2)) +
            np.sqrt(np.square(x2 - x3) + np.square(y2 - y3)) +
            np.sqrt(np.square(x3 - x1) + np.square(y3 - y1)))


def create_planets_dataset(total_days, distances, degrees):
    """
    Creates a dataset with the positions and characteristics of the configuration of the planets
    for a total given of days and an array of distance of the planets from the sun
    :param total_days int Number of days to crate the dataset for.
    :param distances list List of distances of planets to the sun.
    :param steps list List of degrees in which each planet moves everyday.
    """

    # pos_X stands for the position of planets Ferengi, Betasoide and Vulcano respectively
    df = pd.DataFrame(index=np.arange(total_days))
    df['pos_f'] = (df.index * degrees[0]) % 360
    df['pos_b'] = (df.index * degrees[1]) % 360
    df['pos_v'] = (df.index * degrees[2]) % 360

    # Change position to radians
    df[['pos_f', 'pos_b', 'pos_v']] = np.radians(df[['pos_f', 'pos_b', 'pos_v']])

    # Calculate (x,y) of each planet at each position
    df[['x_f', 'x_b', 'x_v']] = np.cos(df[['pos_f', 'pos_b', 'pos_v']]) * distances
    df[['y_f', 'y_b', 'y_v']] = np.sin(df[['pos_f', 'pos_b', 'pos_v']]) * distances

    # calculate area of triangle. If area == 0 then, they are collinear.
    # round the results to be able to compare against 0 and max.
    df['area'] = np.around(
        area_triangle(df['x_f'], df['y_f'], df['x_b'], df['y_b'], df['x_v'], df['y_v'])
        , decimals=5)

    # calculate the perimeter of each triangle
    df['perimeter'] = np.around(
        perimeter_triangle(df['x_f'], df['y_f'], df['x_b'], df['y_b'], df['x_v'], df['y_v']),
        decimals=5)
    # Set perimeter to 0 if area is 0
    df['perimeter'] = df['perimeter'] * (df['area'] > 0)

    # The triangle "excludes" the sun if all the points lay in one half of the plane 
    # or if the sum of the areas of the 3 triangles formed by FB0, BV0, and VF0 equals the area of FBV
    df['sun_inside_tria'] = (df['area'] == np.around(
        area_triangle(df['x_f'], df['y_f'], df['x_b'], df['y_b'], np.zeros(len(df)), np.zeros(len(df))) +
        area_triangle(df['x_b'], df['y_b'], df['x_v'], df['y_v'], np.zeros(len(df)), np.zeros(len(df))) +
        area_triangle(df['x_v'], df['y_v'], df['x_f'], df['y_f'], np.zeros(len(df)), np.zeros(len(df))),
        decimals=5))

    # The planets are in line with the sun when the sun lies "inside the triangle" 
    # and the "area" of the triangle equals 0
    df['in_line_sun'] = (df['sun_inside_tria'] == True) & (df['area'] == 0)

    df = df[['area', 'perimeter', 'sun_inside_tria', 'in_line_sun']]

    return df


def add_weather_to_planets_dataset(df):
    """
    Calculates and appends weather patters to a dataframe containing the configuration
    characteristics of the planets
    """

    # By default the weather is unknown
    df['weather'] = 'unknown'

    # When the planets are inline with the sun, the weather is dry
    df.loc[df['in_line_sun'], 'weather'] = "dry"

    # When the planets form a triangle and the sun lies inside of it, the weather is rainy
    df.loc[df['sun_inside_tria'] & (df['area'] > 0), 'weather'] = "rain"

    # When the weather is rainy and the perimeter of the triangle is maximum, there is
    # a peak in the rain
    df['rain_peak'] = (df['perimeter'] == df['perimeter'].max()) & (df['weather'] == 'rain')

    # When the planets are inline with each other but not with the sun, the weather is optimal
    df.loc[(df['area'] == 0) & ~(df['in_line_sun']), 'weather'] = "optimal"

    return df


def create_weather_dataset(total_days, distances=[500, 2000, 1000], degrees=[1, 3, -5]):
    """
    Creates a weather dataset for a total given of days and an array of distance of
    the planets from the sun
    
    :param total_days int Number of days to crate the dataset for.
    :param distances list List of distances of planets to the sun.
    :param steps list List of degrees in which each planet moves everyday.
    """
    df = create_planets_dataset(total_days, distances, degrees)

    df = add_weather_to_planets_dataset(df)

    return df


if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    # Input Arguments
    parser.add_argument(
        '--days',
        help='Number of days to calculate the weather for',
        required=False,
        type=int,
        default=3600
    )

    parser.add_argument(
        '--distances',
        help='List of distances of planets to the sun.',
        nargs=3,
        required=False,
        type=float,
        default=[500, 2000, 1000]
    )

    parser.add_argument(
        '--degrees',
        help='List of degrees in which each planet moves everyday',
        nargs=3,
        required=False,
        type=float,
        default=[1, -5, 3]
    )

    args = parser.parse_args()
    arguments = args.__dict__

    # Unused args provided by service
    days = arguments.pop('days')
    distances = arguments.pop('distances')
    degrees = arguments.pop('degrees')

    df = create_weather_dataset(days, distances, degrees)

    print("1. ¿Cuántos períodos de sequía habrá?")
    print(len(df[df['weather'] == 'dry']))

    print("2.a ¿Cuántos períodos de lluvia habrá?")
    print(len(df[df['weather'] == 'rain']))

    print("2.b ¿Qué día(s) será(n) el pico máximo de lluvia?")
    print(df[df['rain_peak'] == True].index.to_list())

    print("3. ¿Cuántos períodos de condiciones óptimas de presión y temperatura habrá?")
    print(len(df[df['weather'] == 'optimal']))
