# Contents

- model.py: model and main to run the exercise locally
- load_task.py: task to generate and load the Dataframe containing the weatherforcast to BigQuery
- main.py: contains the cloud function to run on Google Cloud Platform  

# Run locally
1. Create and activate Virtual Environment
   
    ```
    python3 -m venv env
    source env/bin/activate
    ```    
    
1. Install requirements
    
    ```
    pip install -r requirements.txt
    ```
    
1. Run the example

    With default arguments:
    
    ```
    python model.py
    ```
    
    Result:
    
    ```
    1. ¿Cuántos períodos de sequía habrá?
    40
    2.a ¿Cuántos períodos de lluvia habrá?
    1200
    2.b ¿Qué día(s) será(n) el pico máximo de lluvia?
    [72, 108, 252, 288, 432, 468, 612, 648, 792, 828, 972, 1008, 1152, 1188, 1332, 1368, 1512, 1548, 1692, 1728, 1872, 1908, 2052, 2088, 2232, 2268, 2412, 2448, 2592, 2628, 2772, 2808, 2952, 2988, 3132, 3168, 3312, 3348, 3492, 3528]
    3. ¿Cuántos períodos de condiciones óptimas de presión y temperatura habrá?
    0
    ```
    
    Or with different arguments:
    
    ```
    python model.py --days 3600 --distances 500 1931.85165257 1000 --degrees 90 165 150
    ```
    
    Result:
    
    ```
    1. ¿Cuántos períodos de sequía habrá?
    300
    2.a ¿Cuántos períodos de lluvia habrá?
    900
    2.b ¿Qué día(s) será(n) el pico máximo de lluvia?
    [8, 16, 32, 40, 56, 64, 80, 88, 104, 112, 128, 136, 152, 160, 176, 184, 200, 208, 224, 232, 248, 256, 272, 280, 296, 304, 320, 328, 344, 352, 368, 376, 392, 400, 416, 424, 440, 448, 464, 472, 488, 496, 512, 520, 536, 544, 560, 568, 584, 592, 608, 616, 632, 640, 656, 664, 680, 688, 704, 712, 728, 736, 752, 760, 776, 784, 800, 808, 824, 832, 848, 856, 872, 880, 896, 904, 920, 928, 944, 952, 968, 976, 992, 1000, 1016, 1024, 1040, 1048, 1064, 1072, 1088, 1096, 1112, 1120, 1136, 1144, 1160, 1168, 1184, 1192, 1208, 1216, 1232, 1240, 1256, 1264, 1280, 1288, 1304, 1312, 1328, 1336, 1352, 1360, 1376, 1384, 1400, 1408, 1424, 1432, 1448, 1456, 1472, 1480, 1496, 1504, 1520, 1528, 1544, 1552, 1568, 1576, 1592, 1600, 1616, 1624, 1640, 1648, 1664, 1672, 1688, 1696, 1712, 1720, 1736, 1744, 1760, 1768, 1784, 1792, 1808, 1816, 1832, 1840, 1856, 1864, 1880, 1888, 1904, 1912, 1928, 1936, 1952, 1960, 1976, 1984, 2000, 2008, 2024, 2032, 2048, 2056, 2072, 2080, 2096, 2104, 2120, 2128, 2144, 2152, 2168, 2176, 2192, 2200, 2216, 2224, 2240, 2248, 2264, 2272, 2288, 2296, 2312, 2320, 2336, 2344, 2360, 2368, 2384, 2392, 2408, 2416, 2432, 2440, 2456, 2464, 2480, 2488, 2504, 2512, 2528, 2536, 2552, 2560, 2576, 2584, 2600, 2608, 2624, 2632, 2648, 2656, 2672, 2680, 2696, 2704, 2720, 2728, 2744, 2752, 2768, 2776, 2792, 2800, 2816, 2824, 2840, 2848, 2864, 2872, 2888, 2896, 2912, 2920, 2936, 2944, 2960, 2968, 2984, 2992, 3008, 3016, 3032, 3040, 3056, 3064, 3080, 3088, 3104, 3112, 3128, 3136, 3152, 3160, 3176, 3184, 3200, 3208, 3224, 3232, 3248, 3256, 3272, 3280, 3296, 3304, 3320, 3328, 3344, 3352, 3368, 3376, 3392, 3400, 3416, 3424, 3440, 3448, 3464, 3472, 3488, 3496, 3512, 3520, 3536, 3544, 3560, 3568, 3584, 3592]
    3. ¿Cuántos períodos de condiciones óptimas de presión y temperatura habrá?
    300
    ```


# Perform requests

Query https://us-central1-melisolarsystem.cloudfunctions.net/forecast_get?day=90

which will return responses like:

```bash
{"day":90,"weather":"dry"}
```

# Deploy on Gooogle Cloud Platform
1. Create project

    Create a project in Google Cloud Platform. named melisolarsystem as an example

1. Set up Google Cloud SDK

    See https://cloud.google.com/sdk/install

1. Create dataset

    Create a dataset from Cloud Shell
    
    ```
    bq --location=US mk -d \
    --default_table_expiration 3600000 \
    --description "Solar System Dataset" \
    solarsystem
    ```

1. Create a table in BigQuery

    ```
    CREATE TABLE solarsystem.forecast(
      day INT64 NOT NULL, 
      weather STRING
      )
     OPTIONS(
       expiration_timestamp=TIMESTAMP "2020-01-01 00:00:00 UTC",
       description="Weather forecast table"
     )
    ```
    
 1. Load dataset to BigQuery
 
    Edit the project and/or table name in load_task.py
 
    Run load_task.py to load the dataset into BigQuery.
    
    ```
    python load_task.py
    ```
    
    Use arguments to set the distance, degrees and days to compute
    
     ```
    python load_task.py --days 7200 --distances 500 1931.85165257 1000 --degrees 90 165 150
    ```
 
 1. Deploy app as a cloud function
 
     ```
     gcloud functions deploy forecast_get --runtime python37 --trigger-http
     ```
