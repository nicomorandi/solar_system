from flask import abort
from flask import jsonify
from google.cloud import bigquery

#
# File for deploying Cloud Functio to GCP
# table_id must be setup the table_id according to your project name and table
#


# [START functions_forecast_get]
def forecast_get(request):
    """HTTP Cloud Function.
    Returns:
        JSON response containing day and weather.
        If weather for the given day is not found, it returns 404
    """

    if request.method == 'GET':
        request_args = request.args

        if request_args and 'day' in request_args:
            try:
                day = int(request_args['day'])
            except ValueError:
                return abort(400)

            client = bigquery.Client()

            # setup the table_id according to your project name and table
            table_id = "melisolarsystem.solarsystem.forecast"

            query_job = client.query("""
                SELECT day, weather
                FROM {}
                WHERE day = {}
                LIMIT 1""".format(table_id, day))

            results = query_job.result()  # Waits for job to complete.

            if results.total_rows > 0:
                for row in results:
                    res = {'day': row.day, 'weather': row.weather}
                    break
                return jsonify(res)
            else:
                # No rows for that given day
                return abort(404)
        else:
            # day is mandatory
            return abort(400)
    else:
        # Methods other than GET are not allowed
        return abort(405)


# [END functions_forecast_get]
