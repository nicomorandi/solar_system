from google.cloud import bigquery
from model import create_planets_dataset, add_weather_to_planets_dataset, create_weather_dataset
import argparse
import pandas

#
# File for uploading a weather forcast dataset into BigQuery
# table_id must be setup the table_id according to your project name and table
#

if __name__ == '__main__':
    parser = argparse.ArgumentParser()

    # Input Arguments
    parser.add_argument(
        '--days',
        help='Number of days to calculate the weather for',
        required=False,
        type=int,
        default=3600
    )

    parser.add_argument(
        '--distances',
        help='List of distances of planets to the sun.',
        nargs=3,
        required=False,
        type=float,
        default=[500, 2000, 1000]
    )

    parser.add_argument(
        '--degrees',
        help='List of degrees in which each planet moves everyday',
        nargs=3,
        required=False,
        type=float,
        default=[1, -5, 3]
    )

    args = parser.parse_args()
    arguments = args.__dict__

    # Unused args provided by service
    days = arguments.pop('days')
    distances = arguments.pop('distances')
    degrees = arguments.pop('degrees')

    dataframe = create_weather_dataset(days, distances, degrees)

    client = bigquery.Client()

    # Setup the table_id according to your project name and table
    table_id = "melisolarsystem.solarsystem.forecast"

    dataframe['day'] = dataframe.index
    dataframe = dataframe[['day', 'weather']]

    job_config = bigquery.LoadJobConfig(
        schema=[
            bigquery.SchemaField("weather", bigquery.enums.SqlTypeNames.STRING),
        ],
        write_disposition="WRITE_TRUNCATE",
    )

    job = client.load_table_from_dataframe(
        dataframe,
        table_id,
        job_config=job_config,
        location="US",
    )
    job.result()

    table = client.get_table(table_id)
    print(
        "Loaded {} rows and {} columns to {}".format(
            table.num_rows, len(table.schema), table_id
        )
    )
